package password;

/**
 * 
 * @author Daniel Nguyen
 * assumed spaces should not be considered valid characters in the password
 * assumed no password will be counted as failed
 * 
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT = 2;
	
	public static boolean IsValidLength(String password) {
		return (password != null && password.length()>= MIN_LENGTH && !password.contains(" "));
	}
	public static boolean HasValidDigitCount(String password) {
		int count = 0;
		for(int i =0;i<password.length();i++) {	
			if(Character.isDigit(password.charAt(i))) {
				count++;
			}
		}
		return (count>=MIN_DIGIT)? true : false;
	}
	public static boolean HasValidUppercaseCount(String password) {
		return password.matches(".*[A-Z]{2,}.*");
	}
}
