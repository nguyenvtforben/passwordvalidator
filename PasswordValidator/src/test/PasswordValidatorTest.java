package test;

import static org.junit.Assert.*;

import org.junit.Test;

import password.PasswordValidator;

public class PasswordValidatorTest {

	/*
	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.IsValidLength("sdrew3st623");
		assertTrue("Password too short", result);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.IsValidLength("");
		assertFalse("Password too short",result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.IsValidLength("1234567");
		assertFalse("Password too short",result);
		
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.IsValidLength("abcdef12");
		assertTrue("Password too short",result);
	}
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean result = PasswordValidator.IsValidLength("Hello There");
		assertFalse("Password too short",result);
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean result = PasswordValidator.IsValidLength(null);
		assertFalse("Password too short",result);
	}
	*/
	
//	@Test
//	public void testHasValidDigitCountRegular() {
//		boolean result = PasswordValidator.HasValidDigitCount("2343ewrasdfdw");
//		assertTrue("Not Enough Digits",result);
//	}
//	
//	@Test
//	public void testHasValidDigitCountException() {
//		boolean result = PasswordValidator.HasValidDigitCount("sdfsdfdsfsd");
//		assertFalse("Not Enough Digits",result);
//	}
//	
//	@Test
//	public void testHasValidDigitCountBoundaryIn() {
//		boolean result = PasswordValidator.HasValidDigitCount("23sdfd");
//		assertTrue("Not Enough Digits",result);
//	}
//	
//	@Test
//	public void testHasValidDigitCountBoundaryOut() {
//		boolean result = PasswordValidator.HasValidDigitCount("1asdf");
//		assertFalse("Not Enough Digits",result);
//	}
	
	@Test
	public void testHasValidUppercaseCountRegular() {
		boolean result = PasswordValidator.HasValidUppercaseCount("ASD23dfgds");
		assertTrue("Not Enough Digits",result);
	}
	
	@Test
	public void testHasValidUppercaseCountBoundaryIn() {
		boolean result = PasswordValidator.HasValidUppercaseCount("TUan");
		assertTrue("Not Enough Digits",result);
	}
	
	@Test
	public void testHasValidUppercaseCountBoundaryOut() {
		boolean result = PasswordValidator.HasValidUppercaseCount("Hello");
		assertFalse("Not Enough Digits",result);
	}
	//
	@Test
	public void testHasValidUppercaseCountException() {
		boolean result = PasswordValidator.HasValidUppercaseCount("nice");
		assertFalse("Not Enough Digits",result);
	}
	

}
